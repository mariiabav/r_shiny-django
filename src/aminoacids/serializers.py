from rest_framework import serializers  # type: ignore
from aminoacids.models import Africa


class AfricaDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Africa
        fields = '__all__'
