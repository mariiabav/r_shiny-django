from rest_framework import generics  # type: ignore
from aminoacids.serializers import AfricaDetailSerializer
from aminoacids.models import Africa


# Create your views here.
class AfricaCreateView(generics.CreateAPIView):
    serializer_class = AfricaDetailSerializer


class AfricaListView(generics.ListAPIView):
    serializer_class = AfricaDetailSerializer
    queryset = Africa.objects.all()


class AfricaDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = AfricaDetailSerializer
    queryset = Africa.objects.all()
