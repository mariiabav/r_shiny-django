from django.db import models  # type: ignore


# Create your models here.
class Africa(models.Model):
    file = models.IntegerField(verbose_name="file",
                               primary_key=True, unique=True)
    K = models.CharField(verbose_name="K", max_length=64)
    m1 = models.CharField(verbose_name="m1", max_length=64)
    m2 = models.CharField(verbose_name="m2", max_length=64)
