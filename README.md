# Server work & visualization
## Purpose
Application is used for visualization of server data.  
The server data consists of observed coefficients `m1`, `m2`, `K` for aminoacids from observations with numbers 1..15. Amino acids research has been conducted in Africa.
## Requirements
- [python 3.8.x](https://www.python.org/downloads/) or higher 
- additional packages (see in `requirements.txt`) 
- [R](https://cran.r-project.org/) version 4.0.3 or later  
- [RStudio](https://rstudio.com/products/rstudio/download/) latest version
## Installation
- Download these files from GitLab or clone this repository via https (don't forget to install git):
    ```
    git clone https://gitlab.com/mariiabav/r_shiny-django.git
    cd r_shiny-django
    ```
    If you want to run the application from the develop branch:
    ```
    git checkout develop
    ```
- Install dependencies for command line application. Use absolute path to `requirements.txt` file (use full paths if necessary)
   ```
   pip3 install -r requirements.txt
   ```
   If you use PyCharm, you can run this command from `r_shiny-django` project Terminal. Also don't forget to set `python 3.8.x` (or higher) as project interpreter.
- Open RStudio and install following packages in console:
  ```
  install.packages("shiny")
  install.packages("httr")
  ```
  
## Usage
- Run server from the root folder of repository. Use the commands (use full paths if necessary):
   ```
   cd src
   python3 manage.py runserver
   ```
- **Mac:** Open second command prompt.  
   Right-click the command prompt window icon and select Command Prompt. A second command prompt window is opened.  
   Start shiny application from the second cmd. Use the commands (use full paths if necessary):
   ```
   cd r_shiny-django/src # use full path
   RScript -e 'library(methods); shiny::runApp("rshiny/", launch.browser = T)'
   ```
- **Windows:** You can use RStudio to run the application. Open `ui.R` and `server.R` from folder `src/rshiny` and click on "Run App":  
   <img src = https://sun9-59.userapi.com/ZsF39J7BM-g9Qgu43iKaXTaz3v7yKmlzbi6KQg/noHuPBMhL58.jpg width ="675" height = "721">
## Example
Change the number of coefficients using slider to see the visualization of server data:
<img src="https://sun9-37.userapi.com/impf/m2EXaxLh8HBpnbOI9hjoI9BlWYmqgu1EEvtfMQ/WRROcqTzqG4.jpg?size=898x454&quality=96&proxy=1&sign=fe35bba6dc2eeec44d89bcf3e9d76fe0"  width="1027" height="457">

<img src="https://sun9-3.userapi.com/impf/OHyKJI5Yl5bAaPHTEEs5SUoxsrvQvCfWu_CaLA/5Z_WJfNZslU.jpg?size=896x449&quality=96&proxy=1&sign=7c1284c959998d117b50a69d0ddaad34"  width="1027" height="457">

<img src="https://sun9-14.userapi.com/impf/guM1-MeZh3OcpYYNCgS1GMOr3qP9PN-JBcZOzQ/CKmEQLbfRq0.jpg?size=899x452&quality=96&proxy=1&sign=3973d0de9087301c8f183b32fa50bdd1"  width="1027" height="457">




